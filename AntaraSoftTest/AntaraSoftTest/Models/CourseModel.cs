﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Data.Entity;

namespace AntaraSoftTest.Models
{
    public class Course
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Display(Name = "Название Курса")]
        [Required(ErrorMessage = "Введите название Курса")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть от 2 до 50 символов")]
        [Remote("CheckCourseName", "Required", ErrorMessage = "Такой Курс уже существует, введите другое название", AdditionalFields = "InitialCourseName")]
        public string Name { get; set; }

        [Display(Name = "Цена в грн.")]
        [Required(ErrorMessage = "Введите стоимость Курса")]
        [DataType(DataType.Currency)]
        [Range(typeof(int), "0", "100000")]
        public int Cost { get; set; }

        public int DateCourseId { get; set; }
        [Display(Name = "День недели")]
        public DateCourse DateCourse { get; set; }

        public int TimeStartId { get; set; }
        [Display(Name = "Начало курса")]
        public TimeStart TimeStart { get; set; }

        public int TimeEndId { get; set; }
        [Display(Name = "Окончание курса")]
        public TimeEnd TimeEnd { get; set; }
    }

    public class DateCourse
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Display(Name = "День недели")]
        public string Name { get; set; }
    }

    public class TimeStart
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Display(Name = "Начало курса")]
        public string Name { get; set; }
    }

    public class TimeEnd
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Display(Name = "Окончание курса")]
        public string Name { get; set; }
    }

    public class CourseContext : DbContext
    {
        public DbSet<Course> Courses { get; set; }
        public DbSet<DateCourse> DateCourses { get; set; }
        public DbSet<TimeStart> TimeStarts { get; set; }
        public DbSet<TimeEnd> TimeEnds { get; set; }
    }
}