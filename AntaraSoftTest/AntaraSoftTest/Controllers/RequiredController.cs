﻿using System.Linq;
using System.Web.Mvc;

namespace AntaraSoftTest.Controllers
{
    public class RequiredController : CustomController
    {       
        [HttpGet]
        public JsonResult CheckCourseName(string Name, string InitialCourseName)
        {
            bool result = false;
            if (Name.ToLower().TrimEnd().TrimStart() == InitialCourseName.ToLower().TrimEnd().TrimStart())
            {
                result = true;
            }
            else result = !(db.Courses.Where(p => p.Name == Name.TrimEnd().TrimStart())).Any();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}