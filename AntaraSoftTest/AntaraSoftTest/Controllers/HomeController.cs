﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AntaraSoftTest.Models;

namespace AntaraSoftTest.Controllers
{
    public class HomeController : CustomController
    {
        public ActionResult Index()
        {
            var course = db.Courses.Include(p => p.DateCourse).Include(k => k.TimeStart).Include(i => i.TimeEnd);
            return View(course.ToList());
        }

        [HttpGet]
        public ActionResult Create()
        {
            SelectList dates = new SelectList(db.DateCourses, "Id", "Name");
            SelectList timeStarts = new SelectList(db.TimeStarts, "Id", "Name");
            SelectList timeEnds = new SelectList(db.TimeEnds, "Id", "Name");
            ViewBag.Dates = dates;
            ViewBag.TimeStarts = timeStarts;
            ViewBag.TimeEnds = timeEnds;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Course course)
        {
            if (course.TimeEndId <= course.TimeStartId)
            {
                ModelState.AddModelError("TimeEndId", "Время окончания должно быть больше времени начала");
            }
            if (ModelState.IsValid)
            {
                db.Courses.Add(course);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            SelectList dates = new SelectList(db.DateCourses, "Id", "Name");
            SelectList timeStarts = new SelectList(db.TimeStarts, "Id", "Name");
            SelectList timeEnds = new SelectList(db.TimeEnds, "Id", "Name");
            ViewBag.Dates = dates;
            ViewBag.TimeStarts = timeStarts;
            ViewBag.TimeEnds = timeEnds;
            return View(course);
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = db.Courses.Find(id);
            if (course == null)
            {
                return HttpNotFound();
            }
            SelectList dates = new SelectList(db.DateCourses, "Id", "Name");
            SelectList timeStarts = new SelectList(db.TimeStarts, "Id", "Name");
            SelectList timeEnds = new SelectList(db.TimeEnds, "Id", "Name");
            ViewBag.Dates = dates;
            ViewBag.TimeStarts = timeStarts;
            ViewBag.TimeEnds = timeEnds;
            return View(course);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Course course)
        {
            if (course.TimeEndId <= course.TimeStartId)
            {
                ModelState.AddModelError("TimeEndId", "Время окончания должно быть больше времени начала");
            }
            if (ModelState.IsValid)
            {
                db.Entry(course).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            SelectList dates = new SelectList(db.DateCourses, "Id", "Name");
            SelectList timeStarts = new SelectList(db.TimeStarts, "Id", "Name");
            SelectList timeEnds = new SelectList(db.TimeEnds, "Id", "Name");
            ViewBag.Dates = dates;
            ViewBag.TimeStarts = timeStarts;
            ViewBag.TimeEnds = timeEnds;
            return View(course);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = db.Courses.Find(id);
            if (course == null)
            {
                return HttpNotFound();
            }
            return View(course);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Course course = db.Courses.Find(id);
            db.Courses.Remove(course);
            db.SaveChanges();
            return RedirectToAction("Index");
        }        
    }
}