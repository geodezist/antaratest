﻿using System.Web.Mvc;
using AntaraSoftTest.Models;

namespace AntaraSoftTest.Controllers
{
    public class CustomController : Controller
    {
        public CourseContext db = new CourseContext();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}